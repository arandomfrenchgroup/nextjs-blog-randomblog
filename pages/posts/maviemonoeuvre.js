import Head from 'next/head'
import Link from 'next/link'


export default function Home() {
  return (
    <div className="container">
      <Head>
        <title>Next Page LOL</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <h1>BollingBrains</h1>
        <Link href="/ma-bite-cest-du-poulet">
            <a>J'adore AsterionRL, allez le checker ici </a>
        </Link>
      </main>

      <footer>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className="logo" />
        </a>
      </footer>
    </div>
  )
}
